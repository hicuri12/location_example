# location_example


## flutter doctor
$ flutter doctor -v
[√] Flutter (Channel stable, 3.10.4, on Microsoft Windows [Version
    10.0.22621.1992], locale ko-KR)
    • Flutter version 3.10.4 on channel stable at C:\flutter
    • Upstream repository https://github.com/flutter/flutter.git
    • Framework revision 682aa387cf (7 weeks ago), 2023-06-05 18:04:56    
      -0500
    • Engine revision 2a3401c9bb
    • Dart version 3.0.3
    • DevTools version 2.23.1

[√] Windows Version (Installed version of Windows is version 10 or higher)

[√] Android toolchain - develop for Android devices (Android SDK version
    33.0.0)
    • Android SDK at C:\Users\user\AppData\Local\Android\Sdk
    • Platform android-TiramisuPrivacySandbox, build-tools 33.0.0
    • Java binary at: C:\Program Files\Android\Android Studio\jre\bin\java
    • Java version OpenJDK Runtime Environment (build
      11.0.15+0-b2043.56-9505619)
    • All Android licenses accepted.

[√] Chrome - develop for the web
    • Chrome at C:\Program Files\Google\Chrome\Application\chrome.exe     

[√] Android Studio (version 2022.1)
    • Android Studio at C:\Program Files\Android\Android Studio
    • Flutter plugin can be installed from:
       https://plugins.jetbrains.com/plugin/9212-flutter
    • Dart plugin can be installed from:
       https://plugins.jetbrains.com/plugin/6351-dart
    • Java version OpenJDK Runtime Environment (build
      11.0.15+0-b2043.56-9505619)

[√] VS Code (version 1.80.1)
    • VS Code at C:\Users\user\AppData\Local\Programs\Microsoft VS Code   
    • Flutter extension version 3.68.0

[√] Connected device (4 available)
    • sdk gphone64 x86 64 (mobile) • emulator-5554 • android-x64    •     
      Android 13 (API 33) (emulator)
    • Windows (desktop)            • windows       • windows-x64    •     
      Microsoft Windows [Version 10.0.22621.1992]
    • Chrome (web)                 • chrome        • web-javascript •     
      Google Chrome 114.0.5735.199
    • Edge (web)                   • edge          • web-javascript •     
      Microsoft Edge 115.0.1901.183

[√] Network resources
    • All expected network resources are available.



## settings



# Android
- 정상적인 빌드를 위한 아래 경로의 signing key 셋팅
```
location_example\android\key.properties
storePassword=
keyPassword=
``````

- 구글 맵 api 를 사용하기 위한 api-key 적용
```
location_example/android/app/src/main/AndroidManifest.xml

        <meta-data
            android:name="com.google.android.geo.API_KEY"
            android:value="YOUR-API-KEY" />

```

# IOS
```
location_example/ios/Runner/AppDelegate.swift

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GMSServices.provideAPIKey("YOUR-API-KEY")
    GeneratedPluginRegistrant.register(with: self)

    if #available(iOS 10.0, *) {
      UNUserNotificationCenter.current().delegate = self
    }

    BackgroundLocationTrackerPlugin.setPluginRegistrantCallback { registry in
        GeneratedPluginRegistrant.register(with: registry)
    }
    
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
```





## location library
- 백그라운드 및 포그라운드 실행을 위해 background_location_tracker 를 사용
- main.dart 다음의 코드를 활용하여 적용
```
@pragma('vm:entry-point')
void backgroundCallback() {
  BackgroundLocationTrackerManager.handleBackgroundUpdated(
    (data) async {
      await AppStatusService.addData(data: data);
    },
  );
}

... 생략 ...


await BackgroundLocationTrackerManager.initialize(
  backgroundCallback,
  config: const BackgroundLocationTrackerConfig(
    loggingEnabled: false,
    androidConfig: AndroidConfig(
      notificationIcon: 'explore',
      trackingInterval: Duration(seconds: 1),
      distanceFilterMeters: 0,
    ),
    iOSConfig: IOSConfig(
      activityType: ActivityType.FITNESS,
      distanceFilterMeters: null,
      restartAfterKill: true,
    ),
  ),
);
LocationService locationService = LocationService();
await locationService.startTracking();
```


## 기초 로직 및 설명
- 수동으로 실행 할 수 있다
- 이동속도가 15Km/h 이상이면 주행모드로 자동실행된다
- 이동속도가 3Km/h 미만으로 5분이상 서 있다면 자동 종료된다
- background_location_tracker 의 경우 사용자의 위치를 약 1초에 한번 수집하므로 최근 5개 위치를 통해 거리를 알아내고 거리와 시간을 통해 속도를 유추한다
- 수집 데이터는 기록시간, 이동거리, 순간속도
- 이동 여부를 확인하기 위해 앱이 활성화 되어있는 경우 google_map_flutter 를 활용하여 표현
- 실제 데이터는 background_location_tracker에서 수집하여 SQLite로 스마트폰 로컬에 저장처리하며 기록 종료시 서버로 전송처리

## 요청사항은 아래와 같습니다
- 현재 위치를 보정하여 정확한 위치 수집
- 보정된 위치를 통해 정확한 거리와 속도 유추 가능
- 앱을 백그라운드 상태로 보냈을 때 pouse 되지 않고 포그라운드 서비스 형태로 위치를 지속적으로 수집할 수 있음
- flutter를 통해 멀티 플랫폼으로 작동하여야 함


내부적으로 사용한 수식이나 필터들이 있는데, 크게 소용이 없어서 전부 걷어 낸 상태이고
최대한 심플하게 의도만 전달 될 수 있도록 뼈대 코드 형식으로 제공 드립니다.

말씀하셨던 것처럼 모듈을 개발하는 방식으로 접근 해 주시면 될것 같습니다.
예제앱을 만들어서 제공해 주시면 제가 저희 앱에 적용하는 식으로 진행 하겠습니다.
기타 필요한 사항은 집적 연락 주시면 됩니다.
