import 'package:background_location_tracker/background_location_tracker.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter_android/google_maps_flutter_android.dart';
import 'package:google_maps_flutter_platform_interface/google_maps_flutter_platform_interface.dart';
import 'package:location/location.dart';
//import 'package:metagaugedapp/src/interface/no_check_certificate_http.dart';
import 'package:metagaugedapp/src/config/sqlite_config.dart';
import 'package:metagaugedapp/src/provider/app_status_provider.dart';
import 'package:metagaugedapp/src/service/app_status_service.dart';
import 'package:metagaugedapp/src/service/location_service.dart';
import 'package:metagaugedapp/src/util/permission_utils.dart';
import 'package:metagaugedapp/src/view/template/main_template.dart';
import 'package:provider/provider.dart';

@pragma('vm:entry-point')
void backgroundCallback() {
  BackgroundLocationTrackerManager.handleBackgroundUpdated(
    (data) async {
      await AppStatusService.addData(data: data);
    },
  );
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized(); // must be above all

  // self-signed
  //HttpOverrides.global = NoCheckCertificateHttp();

  // notification permission
  final PermissionUtils permissionUtils = PermissionUtils();
  await permissionUtils.requestNotificationStatus().then(
    (granted) async {
      if (!granted) return;
    },
  );

  // location permission
  Location location = Location();
  await location.hasPermission().then(
    (permissionGranted) async {
      if (permissionGranted == PermissionStatus.denied) {
        await location.requestPermission().then((requestResult) {
          if (requestResult != PermissionStatus.granted) {
            return;
          }
        });
      }
    },
  );

  // init background service
  await BackgroundLocationTrackerManager.initialize(
    backgroundCallback,
    config: const BackgroundLocationTrackerConfig(
      loggingEnabled: false,
      androidConfig: AndroidConfig(
        notificationIcon: 'explore',
        trackingInterval: Duration(seconds: 1),
        distanceFilterMeters: 0,
      ),
      iOSConfig: IOSConfig(
        activityType: ActivityType.FITNESS,
        distanceFilterMeters: null,
        restartAfterKill: true,
      ),
    ),
  );
  LocationService locationService = LocationService();
  await locationService.startTracking();

  // sqlite
  SQLiteConfig();

  // google map flutter
  final GoogleMapsFlutterPlatform mapsImplementation =
      GoogleMapsFlutterPlatform.instance;
  if (mapsImplementation is GoogleMapsFlutterAndroid) {
    mapsImplementation.useAndroidViewSurface = true;
    mapsImplementation.initializeWithRenderer(AndroidMapRenderer.latest);
  }

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (BuildContext context) => AppStatusProvider(),
        ),
      ],
      child: MaterialApp(
        theme: ThemeData(fontFamily: 'PretendardVariable'),
        initialRoute: '/',
        routes: {'/': (context) => const MainTemplate()},
      ),
    ),
  );
}
