import 'package:flutter/material.dart';
import 'package:metagaugedapp/src/enum/app_status.dart';
import 'package:metagaugedapp/src/enum/mode_status.dart';
import 'package:metagaugedapp/src/model/status_model.dart';

class AppStatusProvider extends ChangeNotifier {
  AppStatus _appStatus = AppStatus.WAIT;
  AppStatus get appStatus => _appStatus;
  set appStatus(AppStatus value) {
    _appStatus = value;
    notifyListeners();
  }

  ModeStatus _modeStatus = ModeStatus.RUN;
  ModeStatus get modeStatus => _modeStatus;
  set modeStatus(ModeStatus value) {
    _modeStatus = value;
    notifyListeners();
  }

  int _activeTime = 0;
  int get activeTime => _activeTime;
  set activeTime(int value) {
    _activeTime = value;
    notifyListeners();
  }

  int _stayTime = 0;
  int get stayTime => _stayTime;
  set stayTime(int value) {
    _stayTime = value;
    notifyListeners();
  }

  int _calcTime = 0;
  int get calcTime => _calcTime;
  set calcTime(int value) {
    _calcTime = value;
    notifyListeners();
  }

  double _speed = 0.0;
  double get speed => _speed;
  set speed(double value) {
    _speed = value;
    notifyListeners();
  }

  double _distance = 0.0;
  double get distance => _distance;
  set distance(double value) {
    _distance = value;
    notifyListeners();
  }

  bool _isViewMode = false;
  bool get isViewMode => _isViewMode;
  set isViewMode(bool value) {
    _isViewMode = value;
    notifyListeners();
  }

  bool _isActiveMode = false;
  bool get isActiveMode => _isActiveMode;
  set isActiveMode(bool value) {
    _isActiveMode = value;
    notifyListeners();
  }

  void fromStatus({required StatusModel status}) {
    _appStatus = AppStatus.fromStatus(status.app);
    _activeTime = status.startTimestamp;
    _speed = status.speed;
    _distance = status.distance;
    notifyListeners();
  }
}
