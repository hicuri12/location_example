class PathInsertRequestDTO {
  final double distance;
  final int activeTime;
  final String activeType;
  final List<Map> pathData;

  PathInsertRequestDTO({
    required this.distance,
    required this.activeTime,
    required this.activeType,
    required this.pathData,
  });
}
