import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:metagaugedapp/src/repository/sqlite_repository.dart';

class MainTemplate extends StatefulWidget {
  const MainTemplate({super.key});

  @override
  State<MainTemplate> createState() => _MainTemplateState();
}

class _MainTemplateState extends State<MainTemplate> {
  final SQLiteRepository sqliteRepository = SQLiteRepository();
  Location location = Location();
  // 지도 UI
  final Completer<GoogleMapController> _controller = Completer();
  late double deviceWidth;
  late double deviceHeight;
  bool mapInit = false;
  LatLng currentLocation = const LatLng(0.0, 0.0);

  // Location 은 화면 표시를 위해 사용함
  void startTracking() async {
    GoogleMapController controller = await _controller.future;

    location.getLocation().then((data) {
      currentLocation = LatLng(
        data.latitude!,
        data.longitude!,
      );
      mapInit = true;

      controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(
          zoom: 20,
          bearing: 0.0, // 방위
          tilt: 0, // 카메라 기울기
          target: currentLocation,
        ),
      ));
    });

    location.onLocationChanged.listen((data) async {
      currentLocation = LatLng(
        data.latitude!,
        data.longitude!,
      );

      controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            zoom: 17,
            bearing: data.heading ?? 0.0, // 방위
            tilt: 30, // 카메라 기울기
            target: currentLocation,
          ),
        ),
      );

      if (mounted) {
        setState(() {});
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    deviceWidth = MediaQuery.of(context).size.width;
    deviceHeight = MediaQuery.of(context).size.height;

    return WillPopScope(
      child: Scaffold(
        body: SafeArea(
          child: SizedBox(
            width: deviceWidth,
            height: deviceHeight,
            child: GoogleMap(
              mapType: MapType.normal,
              liteModeEnabled: false,
              myLocationEnabled: true,
              myLocationButtonEnabled: true,
              compassEnabled: true,
              trafficEnabled: false,
              buildingsEnabled: true,
              indoorViewEnabled: true,
              mapToolbarEnabled: false,
              zoomControlsEnabled: true,
              tiltGesturesEnabled: true,
              scrollGesturesEnabled: true,
              zoomGesturesEnabled: true,
              rotateGesturesEnabled: true,
              initialCameraPosition: CameraPosition(
                target: currentLocation,
                zoom: 20,
              ),
              minMaxZoomPreference: const MinMaxZoomPreference(10.0, 18),
              onMapCreated: (GoogleMapController controller) async {
                _controller.complete(controller);
                startTracking();
              },
            ),
          ),
        ),
      ),
      onWillPop: () async => false,
    );
  }
}
