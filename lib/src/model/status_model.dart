import 'package:metagaugedapp/src/enum/app_status.dart';
import 'package:metagaugedapp/src/enum/mode_status.dart';

class StatusModel {
  String app;
  String mode;
  double latitude;
  double longitude;
  double speed;
  double distance;
  int startTimestamp;
  int calcTimestamp;
  int stayTimestamp;

  StatusModel({
    required this.app,
    required this.mode,
    required this.latitude,
    required this.longitude,
    required this.speed,
    required this.distance,
    required this.startTimestamp,
    required this.calcTimestamp,
    required this.stayTimestamp,
  });

  StatusModel.fromMap({required Map<dynamic, dynamic> map})
      : app = map['app'],
        mode = map['mode'],
        latitude = map['latitude'],
        longitude = map['longitude'],
        speed = map['speed'],
        distance = map['distance'],
        startTimestamp = map['startTimestamp'],
        calcTimestamp = map['calcTimestamp'],
        stayTimestamp = map['stayTimestamp'];

  StatusModel.fromEmpty()
      : app = AppStatus.WAIT.name,
        mode = ModeStatus.RUN.name,
        latitude = 0.0,
        longitude = 0.0,
        speed = 0.0,
        distance = 0.0,
        startTimestamp = 0,
        calcTimestamp = 0,
        stayTimestamp = 0;

  @override
  String toString() =>
      'StatusModel(app=$app, mode=$mode, latitude=$latitude, longitude=$longitude, speed=$speed, distance=$distance, startTimestamp=$startTimestamp, calcTimestamp=$calcTimestamp, stayTimestamp=$stayTimestamp)';
}
