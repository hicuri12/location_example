import 'dart:convert';

class ActiveData {
  double latitude;
  double longitude;
  double distance;
  double speed;
  int timestamp;

  ActiveData({
    this.latitude = 0.0,
    this.longitude = 0.0,
    this.distance = 0.0,
    this.speed = 0,
    this.timestamp = 0,
  });

  factory ActiveData.fromMap(Map<String, dynamic> map) => ActiveData(
        latitude: map['latitude'],
        longitude: map['longitude'],
        distance: map['distance'],
        speed: map['speed'],
        timestamp: map['timestamp'],
      );

  factory ActiveData.fromJson(String json) {
    dynamic map = jsonDecode(json) as Map<String, dynamic>;
    return ActiveData.fromMap(map);
  }

  @override
  String toString() =>
      'ActiveData(latitude: $latitude, longitude: $longitude, distance: $distance, speed: $speed, timestamp: $timestamp)';

  Map<String, dynamic> toMap() => <String, dynamic>{
        'latitude': latitude,
        'longitude': longitude,
        'distance': distance,
        'speed': speed,
        'timestamp': timestamp,
      };

  String toJsonstring() =>
      '{"latitude": $latitude, "longitude": $longitude, "distance": $distance, "speed": $speed, "timestamp": $timestamp}';
}
