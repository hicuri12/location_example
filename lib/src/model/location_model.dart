class LocationModel {
  double latitude;
  double longitude;
  double altitude;
  double distance;
  double bearing;
  double speed;
  int time;

  LocationModel({
    required this.latitude,
    required this.longitude,
    required this.altitude,
    required this.distance,
    required this.bearing,
    required this.speed,
    required this.time,
  });

  LocationModel.fromMap({required Map<dynamic, dynamic> map})
      : latitude = map['latitude'],
        longitude = map['longitude'],
        altitude = map['altitude'],
        distance = map['distance'],
        bearing = map['bearing'],
        speed = map['speed'],
        time = map['time'];

  LocationModel.fromEmpty()
      : latitude = 0.0,
        longitude = 0.0,
        altitude = 0.0,
        distance = 0.0,
        bearing = 0.0,
        speed = 0.0,
        time = 0;

  @override
  String toString() =>
      'LocationModel(latitude=$latitude, longitude=$longitude, altitude=$altitude, bearing=$bearing, distance=$distance, speed=$speed, time=$time)';
}
