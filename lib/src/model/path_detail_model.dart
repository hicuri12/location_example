class PathDetailModel {
  int duid;
  int puid;
  double latitude;
  double longitude;
  double altitude;
  int speed;
  double distance;
  int bearing;
  double time;

  PathDetailModel({
    required this.duid,
    required this.puid,
    required this.latitude,
    required this.longitude,
    required this.altitude,
    required this.speed,
    required this.distance,
    required this.bearing,
    required this.time,
  });

  PathDetailModel.fromMap(Map<dynamic, dynamic> map)
      : duid = map['duid'],
        puid = map['puid'],
        latitude = map['latitude'],
        longitude = map['longitude'],
        altitude = map['altitude'],
        speed = map['speed'],
        distance = map['distance'],
        bearing = map['bearing'],
        time = map['time'];

  Map<String, dynamic> toMap() {
    return {
      'duid': duid,
      'puid': puid,
      'latitude': latitude,
      'longitude': longitude,
      'altitude': altitude,
      'speed': speed,
      'distance': distance,
      'bearing': bearing,
      'time': time,
    };
  }
}
