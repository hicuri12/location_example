import 'dart:convert';
import 'package:location/location.dart' as loc;
import 'package:background_location_tracker/background_location_tracker.dart';

class LocationData {
  double latitude;
  double longitude;
  double altitude;
  double bearing;
  double speed;
  int time;
  double distance;

  LocationData({
    required this.latitude,
    required this.longitude,
    required this.altitude,
    required this.bearing,
    required this.speed,
    required this.time,
    required this.distance,
  });

  factory LocationData.fromBackgroundLocationUpdateData({
    required BackgroundLocationUpdateData data,
  }) =>
      LocationData(
        latitude: data.lat,
        longitude: data.lon,
        altitude: data.alt,
        bearing: data.course,
        speed: data.speed,
        time: DateTime.now().millisecondsSinceEpoch,
        distance: 0,
      );

  factory LocationData.fromMap(Map<dynamic, dynamic> map) => LocationData(
        latitude: map['latitude'],
        longitude: map['longitude'],
        altitude: map['altitude'],
        bearing: map['bearing'],
        speed: map['speed'],
        time: map['time'],
        distance: map['distance'],
      );

  factory LocationData.fromEmpty() => LocationData(
        latitude: 0.0,
        longitude: 0.0,
        altitude: 0.0,
        bearing: 0.0,
        speed: 0.0,
        time: 0,
        distance: 0.0,
      );

  factory LocationData.fromJsonString(String jsonString) {
    Map<String, dynamic> map = jsonDecode(jsonString);
    return LocationData(
      latitude: map['latitude'],
      longitude: map['longitude'],
      altitude: map['altitude'],
      bearing: map['bearing'],
      speed: map['speed'],
      time: map['time'],
      distance: map['distance'],
    );
  }

  factory LocationData.fromLocationData(loc.LocationData data) {
    return LocationData(
      latitude: data.latitude!,
      longitude: data.longitude!,
      altitude: data.altitude!,
      bearing: data.heading!,
      speed: data.speed!,
      time: data.time!.toInt(),
      distance: 0.0,
    );
  }

  Map<String, dynamic> toMap() => {
        'latitude': latitude,
        'longitude': longitude,
        'altitude': altitude,
        'bearing': bearing,
        'speed': speed,
        'time': time,
        'distance': distance,
      };

  String toJsonString() =>
      '{"latitude" : $latitude, "longitude" : $longitude, "altitude" : $altitude, "bearing" : $bearing, "speed" : $speed, "time" : $time, "distance" : $distance}';

  @override
  String toString() {
    return 'LocationData(latitude : $latitude, longitude : $longitude, altitude : $altitude, bearing : $bearing, speed : $speed, time : $time, distance : $distance)';
  }
}
