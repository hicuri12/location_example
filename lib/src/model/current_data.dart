class CurrentData {
  final double latitude;
  final double longitude;
  final double distance;
  final int bearing;
  final int averageSpeed;
  final int time;

  CurrentData({
    required this.latitude,
    required this.longitude,
    required this.distance,
    required this.bearing,
    required this.averageSpeed,
    required this.time,
  });

  factory CurrentData.fromJson(Map<String, dynamic> map) => CurrentData(
        latitude: map['latitude'],
        longitude: map['longitude'],
        distance: map['distance'],
        bearing: map['bearing'],
        averageSpeed: map['averageSpeed'],
        time: map['time'],
      );

  Map<String, dynamic> toMap() {
    return {
      'latitude': latitude,
      'longitude': longitude,
      'distance': distance,
      'bearing': bearing,
      'averageSpeed': averageSpeed,
      'time': time,
    };
  }

  @override
  String toString() {
    return 'latitude: $latitude, longitude: $longitude, distance: $distance, bearing: $bearing, averageSpeed: $averageSpeed, time: $time';
  }
}
