import 'package:background_location_tracker/background_location_tracker.dart';
import 'package:metagaugedapp/src/tmp/location_dao.dart';

class Repo {
  static Repo? _instance;

  Repo._();

  factory Repo() => _instance ??= Repo._();

  Future<void> update(BackgroundLocationUpdateData data) async {
    await LocationDao().addLocation(data);
  }
}
