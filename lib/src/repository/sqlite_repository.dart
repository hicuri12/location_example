import 'package:metagaugedapp/src/config/sqlite_config.dart';
import 'package:metagaugedapp/src/enum/app_status.dart';
import 'package:metagaugedapp/src/enum/mode_status.dart';
import 'package:metagaugedapp/src/model/location_model.dart';
import 'package:metagaugedapp/src/model/status_model.dart';
import 'package:sqflite/sqflite.dart';

class SQLiteRepository {
  Future<void> insertBuffer({required LocationModel model}) async {
    try {
      Database conn = await SQLiteConfig().database;
      String query = '''
        INSERT INTO tb_buffer (latitude, longitude, altitude, bearing, distance, speed, time)
        VALUES (${model.latitude}, ${model.longitude}, ${model.altitude}, ${model.bearing}, ${model.distance}, ${model.speed}, ${model.time})
      ''';
      await conn.rawInsert(query);
    } catch (e) {
      print('MYLOG ** insertBuffer() exception: ${e.toString()}');
    }
  }

  Future<List<LocationModel>> selectLatestBuffer() async {
    List<LocationModel> result = <LocationModel>[];

    try {
      Database conn = await SQLiteConfig().database;
      String query = 'SELECT * FROM tb_buffer ORDER BY uid DESC LIMIT 5';
      List<Map> rows = await conn.rawQuery(query);
      if (rows.isNotEmpty) {
        for (int i = 0; i < rows.length; i++) {
          result.add(LocationModel.fromMap(map: rows[i]));
        }
      }
    } catch (e) {
      print('MYLOG ** selectLatestBuffer() exception: ${e.toString()}');
    }

    return result;
  }

  Future<int> selectBufferSize() async {
    int result = 0;

    try {
      Database conn = await SQLiteConfig().database;
      String query = 'SELECT COUNT(*) AS count FROM tb_buffer';
      List<Map> rows = await conn.rawQuery(query);
      if (rows.isNotEmpty) {
        result = rows[0]['count'];
      }
    } catch (e) {
      print('MYLOG ** selectLatestBuffer() exception: ${e.toString()}');
    }

    return result;
  }

  Future<double> selectSpeed() async {
    double result = 0.0;

    try {
      Database conn = await SQLiteConfig().database;

      await conn
          .rawQuery('SELECT * FROM tb_buffer ORDER BY uid DESC LIMIT 5')
          .then((rows) {
        if (rows.isNotEmpty) {
          for (Map row in rows) {
            result += row['speed'];
          }
          result /= 5;
        }
      });
    } catch (e) {
      print('MYLOG ** selectSpeed() exception: ${e.toString()}');
    }

    return result;
  }

  Future<List<LocationModel>> selectTemp() async {
    List<LocationModel> result = <LocationModel>[];

    try {
      Database conn = await SQLiteConfig().database;
      String query = 'SELECT * FROM tb_buffer ORDER BY uid DESC LIMIT 5';
      List<Map> rows = await conn.rawQuery(query);

      if (rows.isNotEmpty) {
        for (Map row in rows) {
          result.add(LocationModel.fromMap(map: row));
        }
      }
    } catch (e) {
      print('MYLOG ** selectSpeed() exception: ${e.toString()}');
    }

    return result;
  }

  Future<void> dropBuffer() async {
    try {
      Database conn = await SQLiteConfig().database;
      String dropQuery = 'DROP TABLE IF EXISTS tb_buffer';
      await conn.execute(dropQuery);

      String createQuery = '''
        CREATE TABLE IF NOT EXISTS tb_buffer (
          uid INTEGER PRIMARY KEY AUTOINCREMENT
          , latitude REAL
          , longitude REAL
          , altitude REAL
          , bearing REAL
          , distance REAL
          , speed REAL
          , time INTEGER
        );
      ''';
      await conn.execute(createQuery);
    } catch (e) {
      print('MYLOG ** dropBuffer() exception: ${e.toString()}');
    }
  }

  Future<void> insertActive({required LocationModel model}) async {
    try {
      Database conn = await SQLiteConfig().database;
      String query = '''
        INSERT INTO tb_active (latitude, longitude, altitude, bearing, distance, speed, time)
        VALUES (${model.latitude}, ${model.longitude}, ${model.altitude}, ${model.bearing}, ${model.distance}, ${model.speed}, ${model.time})
      ''';
      await conn.rawInsert(query);
    } catch (e) {
      print('MYLOG ** insertActive() exception: ${e.toString()}');
    }
  }

  Future<List<Map>> selectActive() async {
    List<Map> result = <Map>[];

    try {
      Database conn = await SQLiteConfig().database;
      String query = 'SELECT * FROM tb_active ORDER BY uid ASC';
      List<Map> rows = await conn.rawQuery(query);

      for (var row in rows) {
        result.add(row);
      }
    } catch (e) {
      print('MYLOG ** selectActive() exception: ${e.toString()}');
    }

    return result;
  }

  Future<void> dropActive() async {
    try {
      Database conn = await SQLiteConfig().database;
      String dropQuery = 'DROP TABLE IF EXISTS tb_active';
      await conn.execute(dropQuery);

      String createQuery = '''
        CREATE TABLE IF NOT EXISTS tb_active (
          uid INTEGER PRIMARY KEY AUTOINCREMENT
          , latitude REAL
          , longitude REAL
          , altitude REAL
          , bearing REAL
          , distance REAL
          , speed REAL
          , time INTEGER
        );
      ''';
      await conn.execute(createQuery);
    } catch (e) {
      print('MYLOG ** dropActive() exception: ${e.toString()}');
    }
  }

  Future<StatusModel> selectStatus() async {
    StatusModel result = StatusModel.fromEmpty();
    late List<Map> rows;

    try {
      Database conn = await SQLiteConfig().database;
      String selectQuery = 'SELECT * FROM tb_status';
      rows = await conn.rawQuery(selectQuery);

      if (rows.isEmpty) {
        String insertQuery = '''
          INSERT INTO tb_status (app, mode, latitude, longitude, speed, distance, startTimestamp, calcTimestamp, stayTimestamp)
          VALUES ('${AppStatus.WAIT.name}', '${ModeStatus.RUN.name}', 0.0, 0.0, 0.0, 0.0, 0, 0, 0)
        ''';
        await conn.rawQuery(insertQuery).then((_) async {
          rows = await conn.rawQuery(selectQuery);
        });
      }

      if (rows.isNotEmpty) {
        result = StatusModel.fromMap(map: rows[0]);
      }
    } catch (e) {
      print('MYLOG ** selectStatus() exception: ${e.toString()}');
    }

    return result;
  }

  Future<int> updateStatus({required StatusModel model}) async {
    int result = 0;

    try {
      Database conn = await SQLiteConfig().database;
      String query = '''
        UPDATE tb_status
        SET
        app = '${model.app}'
        , mode = '${model.mode}'
        , latitude = ${model.latitude}
        , longitude = ${model.longitude}
        , speed = ${model.speed}
        , distance = ${model.distance}
        , startTimestamp = ${model.startTimestamp}
        , calcTimestamp = ${model.calcTimestamp}
        , stayTimestamp = ${model.startTimestamp};
      ''';
      result = await conn.rawUpdate(query);
    } catch (e) {
      print('MYLOG ** updateStatus() exception: ${e.toString()}');
    }

    return result;
  }

  Future<void> reset() async {
    try {
      Database conn = await SQLiteConfig().database;
      conn.transaction((txn) async {
        String dropActiveQuery = 'DROP TABLE IF EXISTS tb_active';
        txn.batch().execute(dropActiveQuery);

        String createActiveQuery = '''
          CREATE TABLE IF NOT EXISTS tb_active (
            uid INTEGER PRIMARY KEY AUTOINCREMENT
            , latitude REAL
            , longitude REAL
            , altitude REAL
            , bearing REAL
            , distance REAL
            , speed REAL
            , time INTEGER
          );
        ''';
        txn.batch().execute(createActiveQuery);

        String dropBufferQuery = 'DROP TABLE IF EXISTS tb_buffer';
        txn.batch().execute(dropBufferQuery);

        String createBufferQuery = '''
          CREATE TABLE IF NOT EXISTS tb_buffer (
            uid INTEGER PRIMARY KEY AUTOINCREMENT
            , latitude REAL
            , longitude REAL
            , altitude REAL
            , bearing REAL
            , distance REAL
            , speed REAL
            , time INTEGER
          );
        ''';
        txn.batch().execute(createBufferQuery);

        String upadteStatusQuery = '''
          UPDATE tb_status
          SET
          app = 'WAIT'
          , mode = 'RUN'
          , latitude = 0.0
          , longitude = 0.0
          , speed = 0.0
          , distance = 0.0
          , startTimestamp = 0
          , calcTimestamp = 0
          , stayTimestamp = 0;
        ''';
        txn.batch().rawUpdate(upadteStatusQuery);

        await txn.batch().commit();
      });
    } catch (e) {
      print('MYLOG ** reset() exception: ${e.toString()}');
    }
  }

  /*
  Future<int> insertAll(List<PathModel> pathList) async {
    int result = 0;

    try {
      Database conn = await SQLiteConfig().database;
      String query =
          'INSERT INTO tb_path (mode, st_lat, st_lng, ed_lat, ed_lng, distance, time, status) VALUES ';
      for (PathModel path in pathList) {
        sql +=
            '("${path.mode}", ${path.st_lat}, ${path.st_lng}, ${path.ed_lat}, ${path.ed_lng}, ${path.distance}, ${path.time}, "write"),';
      }

      query = sql.substring(0, sql.lastIndexOf(','));

      result = await conn.rawInsert(sql);
    } catch (e) {
      print(e.toString());
    } finally {
      conn = null;
    }

    return result;
  }

  Future<List<PathModel>> selectAll() async {
    List<PathModel> result = <PathModel>[];

    try {
      Database conn = await SQLiteConfig().database;
      String query = 'SELECT * FROM tb_path';
      List<Map> rows = await conn.rawQuery(sql);

      for (var row in rows) {
        result.add(PathModel.fromMap(row));
      }
    } catch (e) {
      print(e.toString());
    } finally {
      conn = null;
    }

    return result;
  }

  Future<PathModel?> selectLatest(int puid) async {
    PathModel? result;

    try {
      Database conn = await SQLiteConfig().database;
      String query = 'SELECT * FROM tb_path ORDER BY puid DESC LIMIT 1';
      List<Map> rows = await conn.rawQuery(sql);

      if (rows.isNotEmpty) {
        result = PathModel.fromMap(rows[0]);
      }
    } catch (e) {
      print(e.toString());
    } finally {
      conn = null;
    }

    return result;
  }

  Future<PathModel?> selectByPuid(int puid) async {
    PathModel? result;

    try {
      Database conn = await SQLiteConfig().database;
      String query = 'SELECT * FROM tb_path WHERE puid = $puid';
      List<Map> rows = await conn.rawQuery(sql);

      if (rows.isNotEmpty) {
        result = PathModel.fromMap(rows[0]);
      }
    } catch (e) {
      print(e.toString());
    } finally {
      conn = null;
    }

    return result;
  }

  Future<int> update(PathModel path) async {
    int result = 0;

    try {
      Database conn = await SQLiteConfig().database;
      String query =
          'UPDATE tb_path SET mode = "${path.mode}", st_lat = ${path.st_lat}, st_lng = ${path.st_lng}, ed_lat = ${path.ed_lat}, ed_lng = ${path.ed_lng}, distance = ${path.distance}, time = ${path.time}, status = "${path.status}"';
      result = await conn.rawDelete(sql);
    } catch (e) {
      print(e.toString());
    } finally {
      conn = null;
    }

    return result;
  }

  Future<int> deleteAll() async {
    int result = 0;

    try {
      Database conn = await SQLiteConfig().database;
      String query = 'DELETE FROM tb_path';
      result = await conn.rawDelete(sql);
    } catch (e) {
      print(e.toString());
    } finally {
      conn = null;
    }

    return result;
  }

  Future<void> create() async {
    try {
      Database conn = await SQLiteConfig().database;
      String query = '''
        CREATE TABLE IF NOT EXISTS tb_path (
          puid INTEGER PRIMARY KEY AUTOINCREMENT,
          mode TEXT,
          st_lat REAL,
          st_lng REAL,
          ed_lat REAL,
          ed_lng REAL,
          distance REAL,
          time REAL,
          status TEXT
        )
      ''';
      await conn.execute(sql);
    } catch (e) {
      print(e.toString());
    } finally {
      conn = null;
    }
  }

  Future<void> drop() async {
    try {
      Database conn = await SQLiteConfig().database;
      String query = 'DROP TABLE IF EXISTS tb_path';
      await conn.execute(sql);
    } catch (e) {
      print(e.toString());
    } finally {
      conn = null;
    }
  }
  */
}
