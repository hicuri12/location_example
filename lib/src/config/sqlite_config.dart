import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class SQLiteConfig {
  static final SQLiteConfig instance = SQLiteConfig._instance();

  Database? _database;

  SQLiteConfig._instance() {
    _init();
  }

  factory SQLiteConfig() => instance;

  Future<Database> get database async {
    if (_database != null) return _database!;
    await _init();
    return _database!;
  }

  Future<void> _init() async {
    _database = await openDatabase(
      join(await getDatabasesPath(), 'local.db'),
      version: 1,
      onCreate: _onCreate,
      onConfigure: _onConfigure,
    );
  }

  static Future<void> _onCreate(Database db, int version) async {
    String createBufferQuery = '''
      CREATE TABLE IF NOT EXISTS tb_buffer (
        uid INTEGER PRIMARY KEY AUTOINCREMENT
        , latitude REAL
        , longitude REAL
        , altitude REAL
        , bearing REAL
        , distance REAL
        , speed REAL
        , time INTEGER
      );
    ''';
    await db.execute(createBufferQuery);

    String createActiveQuery = '''
      CREATE TABLE IF NOT EXISTS tb_active (
        uid INTEGER PRIMARY KEY AUTOINCREMENT
        , latitude REAL
        , longitude REAL
        , altitude REAL
        , bearing REAL
        , distance REAL
        , speed REAL
        , time INTEGER
      );
    ''';
    await db.execute(createActiveQuery);

    String createStatusQuery = '''
      CREATE TABLE IF NOT EXISTS tb_status (
        app TEXT
        , mode TEXT
        , latitude REAL
        , longitude REAL
        , speed REAL
        , distance REAL
        , startTimestamp INTEGER
        , calcTimestamp INTEGER
        , stayTimestamp INTEGER
      );
    ''';
    await db.execute(createStatusQuery);
  }

  static void _onConfigure(Database db) {}
}
