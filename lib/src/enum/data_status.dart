enum DataStatus {
  WRITE(0, 'WRITE'),
  EXECUTE(1, 'EXECUTE'),
  COMPLETE(2, 'COMPLETE');

  const DataStatus(this.code, this.status);

  final int code;
  final String status;

  factory DataStatus.fromStatus(String status) {
    return DataStatus.values.firstWhere((element) => element.status == status);
  }

  factory DataStatus.fromCode(int code) {
    return DataStatus.values.firstWhere((element) => element.code == code);
  }
}
