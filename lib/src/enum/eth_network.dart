enum EthNetwork {
  ETHEREUM_MAINNET(1, 'Ethereum Mainnet'),
  ROPSTEN_TESTNET(3, 'Ropsten Testnet'),
  RINKEBY_TESTNET(4, 'Rinkeby Testnet'),
  GORELI_TESTNET(5, 'Goreli Testnet'),
  KOVAN_TESTNET(42, 'Kovan Testnet'),
  BSC_MAINNET(56, 'Binance Smart Chain Mainnet'),
  CHAPEL_TESTNET(97, 'Chapel Testnet'),
  POLYGON_MAINNET(137, 'Polygon Mainnet'),
  MUMBAI_TESTNET(80001, 'Mumbai Testnet');

  const EthNetwork(this.code, this.status);

  final int code;
  final String status;

  factory EthNetwork.fromStatus(String status) {
    return EthNetwork.values.firstWhere((element) => element.status == status);
  }

  factory EthNetwork.fromCode(int code) {
    return EthNetwork.values.firstWhere((element) => element.code == code);
  }
}
