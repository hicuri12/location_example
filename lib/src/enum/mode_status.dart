enum ModeStatus {
  RUN(0, 'RUN'),
  DRIVE(1, 'DRIVE'),
  RAIL(2, 'RAIL'),
  FLIGHT(3, 'FLIGHT');

  const ModeStatus(this.code, this.status);

  final int code;
  final String status;

  factory ModeStatus.fromStatus(String status) {
    return ModeStatus.values.firstWhere((element) => element.status == status);
  }

  factory ModeStatus.fromCode(int code) {
    return ModeStatus.values.firstWhere((element) => element.code == code);
  }
}
