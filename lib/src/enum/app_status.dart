enum AppStatus {
  SLEEP(0, 'SLEEP'),
  WAIT(1, 'WAIT'),
  ACTIVE(2, 'ACTIVE'),
  CALC(3, 'CALC');

  const AppStatus(this.code, this.status);

  final int code;
  final String status;

  factory AppStatus.fromStatus(String status) {
    return AppStatus.values.firstWhere((element) => element.status == status);
  }

  factory AppStatus.fromCode(int code) {
    return AppStatus.values.firstWhere((element) => element.code == code);
  }
}
