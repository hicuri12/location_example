import 'dart:math';

class PathUtils {
  static double calculateDistance({
    required double startLatitude,
    required double startLongitude,
    required double endLatitude,
    required double endLongitude,
  }) {
    var p = 0.017453292519943295;
    var a = 0.5 -
        cos((endLatitude - startLatitude) * p) / 2 +
        cos(startLatitude * p) *
            cos(endLatitude * p) *
            (1 - cos((endLongitude - startLongitude) * p)) /
            2;
    return 12742 * asin(sqrt(a));
  }

  // return miter
  static double distanceBetween({
    required double startLatitude,
    required double startLongitude,
    required double endLatitude,
    required double endLongitude,
  }) {
    var p = 0.017453292519943295;
    var a = 0.5 -
        cos((endLatitude - startLatitude) * p) / 2 +
        cos(startLatitude * p) *
            cos(endLatitude * p) *
            (1 - cos((endLongitude - startLongitude) * p)) /
            2;
    return 12742 * asin(sqrt(a));
    /*
    double earthRadius = 6378137.0;
    double dLat = _toRadians(degree: endLatitude - startLatitude);
    double dLon = _toRadians(degree: endLongitude - startLongitude);

    var a = pow(sin(dLat / 2), 2) +
        pow(sin(dLon / 2), 2) *
            cos(_toRadians(degree: startLatitude)) *
            cos(_toRadians(degree: endLatitude));
    var c = 2 * asin(sqrt(a));

    return earthRadius * c;
    */
  }

  static double _toRadians({required double degree}) {
    return degree * pi / 180;
  }
}
