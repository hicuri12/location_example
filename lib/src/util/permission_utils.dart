import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart'
    as PermissionHandler;

class PermissionUtils {
  // notification
  Future<bool> checkNotificationStatus() async {
    bool result = false;
    result = await getNotificationStatus();
    if (!result) {
      // ignore: use_build_context_synchronously
      result = await requestNotificationStatus();
    }

    return result;
  }

  Future<bool> getNotificationStatus() async =>
      await PermissionHandler.Permission.notification.isDenied;
  Future<bool> requestNotificationStatus() async {
    bool result = false;
    await PermissionHandler.Permission.notification.request().then(
      (status) {
        if (!status.isGranted) {
          PermissionHandler.openAppSettings();
        } else {
          result = true;
        }
      },
    );

    return result;
  }

  // location
  Future<bool> checkLocationPermission() async {
    bool result = false;
    result = await getLocationServiceStatus();
    if (!result) {
      // ignore: use_build_context_synchronously
      result = await requestLocationPermission();
    } else {
      result = true;
    }

    return result;
  }

  Future<bool> getLocationServiceStatus() async {
    Location location = Location();
    return await location.serviceEnabled() &&
        await location.hasPermission() == PermissionStatus.granted;
  }

  Future<bool> requestLocationPermission() async {
    bool result = false;
    Location location = Location();
    await location.requestPermission().then(
      (locationPermissionStatus) {
        if (locationPermissionStatus != PermissionStatus.granted) {
          PermissionHandler.openAppSettings();
        } else {
          result = true;
        }
      },
    );

    return result;
  }

  // read & write
  Future<bool> checkReadWriteStatus({required BuildContext context}) async {
    bool result = false;
    result = await getReadWriteStatus();
    if (!result) {
      result = await requestReadWriteStatus(context: context);
    } else {
      result = true;
    }

    return result;
  }

  Future<bool> getReadWriteStatus() async =>
      await PermissionHandler.Permission.storage.isGranted;
  Future<bool> requestReadWriteStatus({required BuildContext context}) async {
    bool result = false;
    await PermissionHandler.Permission.storage.request().then(
      (status) {
        if (!status.isGranted) {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: const Text('읽기/쓰기 권한을 허용 하시겠습니까?'),
                actions: [
                  TextButton(
                    onPressed: () {
                      PermissionHandler.openAppSettings();
                    },
                    child: const Text('설정'),
                  ),
                ],
              );
            },
          );
        } else {
          result = true;
        }
      },
    );

    return result;
  }
}
