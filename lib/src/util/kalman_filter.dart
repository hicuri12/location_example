import 'dart:math' as math;

class KalmanFilter {
  final double _minAccuracy = 1;
  final double _qMetresPerSecond;

  late double _timeStamp;
  late double _latitude;
  late double _longitude;

  // P matrix.  Negative means object uninitialised.  NB: units irrelevant, as long as same units used throughout
  double _variance = -1;

  KalmanFilter(this._qMetresPerSecond);

  double get timeStamp => _timeStamp;

  double get latitude => _latitude;

  double get longitude => _longitude;

  double get accuracy => math.sqrt(_variance);

  void setState(
    double latitude,
    double longitude,
    double accuracy,
    double timeStamp,
  ) {
    _latitude = latitude;
    _longitude = longitude;
    _variance = accuracy * accuracy;
    _timeStamp = timeStamp;
  }

  /*
   * Kalman filter processing for lattitude and longitude.
   * latitude: New measurement of lattidude.
   * longitude: New measurement of longitude.
   * accuracy: Measurement of 1 standard deviation error in metres.
   * timeStamp: Time of measurement.
   * returns: new state.
   */
  void process(
    double latitude,
    double longitude,
    double accuracy,
    double timeStamp,
  ) {
    if (accuracy < _minAccuracy) accuracy = _minAccuracy;
    if (_variance < 0) {
      // if variance < 0, object is unitialised, so initialise with current values
      _timeStamp = timeStamp;
      _latitude = latitude;
      _longitude = longitude;
      _variance = accuracy * accuracy;
    } else {
      // else apply Kalman filter methodology

      double timeIncMilliseconds = timeStamp - _timeStamp;
      if (timeIncMilliseconds > 0) {
        // time has moved on, so the uncertainty in the current position increases
        _variance +=
            timeIncMilliseconds * _qMetresPerSecond * _qMetresPerSecond / 1000;
        _timeStamp = timeStamp;
        // TO DO: USE VELOCITY INFORMATION HERE TO GET A BETTER ESTIMATE OF CURRENT POSITION
      }

      // Kalman gain matrix K = Covarariance * Inverse(Covariance + MeasurementVariance)
      // NB: because K is dimensionless, it doesn't matter that variance has different units to lat and lng
      double K = _variance / (_variance + accuracy * accuracy);
      // apply K
      _latitude += K * (latitude - _latitude);
      _longitude += K * (longitude - _longitude);
      // new Covarariance  matrix is (IdentityMatrix - K) * Covarariance
      _variance = (1 - K) * _variance;
    }
  }
}
