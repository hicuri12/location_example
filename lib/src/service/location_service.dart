import 'dart:async';

import 'package:background_location_tracker/background_location_tracker.dart';
import 'package:permission_handler/permission_handler.dart'
    as PermissionHandler;

class LocationService {
  bool _isTracking = false;

  Future<bool> getTrackingStatus() async {
    _isTracking = await BackgroundLocationTrackerManager.isTracking();
    return _isTracking;
  }

  Future<void> requestLocationPermission() async {
    final result = await PermissionHandler.Permission.locationAlways.request();
    if (result == PermissionHandler.PermissionStatus.granted) {
      print('GRANTED'); // ignore: avoid_print
    } else {
      print('NOT GRANTED'); // ignore: avoid_print
    }
  }

  Future<void> requestNotificationPermission() async {
    final result = await PermissionHandler.Permission.notification.request();
    if (result == PermissionHandler.PermissionStatus.granted) {
      print('GRANTED'); // ignore: avoid_print
    } else {
      print('NOT GRANTED'); // ignore: avoid_print
    }
  }

  Future<void> startTracking() async {
    await BackgroundLocationTrackerManager.startTracking();
    _isTracking = true;
  }

  Future<void> stopTracking() async {
    await BackgroundLocationTrackerManager.stopTracking();
    _isTracking = false;
  }
}
