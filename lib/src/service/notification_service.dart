import 'dart:math';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationService {
  static void send({
    String? title,
    required String body,
  }) {
    final String titleText = title ?? 'Metagauge DApp';

    const settings = InitializationSettings(
      android: AndroidInitializationSettings('app_icon'),
      iOS: DarwinInitializationSettings(
        requestAlertPermission: false,
        requestBadgePermission: false,
        requestSoundPermission: false,
      ),
    );
    FlutterLocalNotificationsPlugin().initialize(
      settings,
      onDidReceiveNotificationResponse: (data) async {
        print('MYLOG ON CLICK $data');
      },
    );
    FlutterLocalNotificationsPlugin().show(
      Random().nextInt(9999),
      titleText,
      body,
      const NotificationDetails(
        android: AndroidNotificationDetails('Metagauge Dapp', 'Message'),
        iOS: DarwinNotificationDetails(),
      ),
    );
  }
}
