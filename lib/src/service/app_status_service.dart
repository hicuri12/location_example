import 'package:background_location_tracker/background_location_tracker.dart';
import 'package:metagaugedapp/src/enum/app_status.dart';
import 'package:metagaugedapp/src/enum/mode_status.dart';
import 'package:metagaugedapp/src/model/location_model.dart';
import 'package:metagaugedapp/src/model/status_model.dart';
import 'package:metagaugedapp/src/repository/sqlite_repository.dart';
import 'package:metagaugedapp/src/service/notification_service.dart';
import 'package:metagaugedapp/src/util/path_utils.dart';

class AppStatusService {
  static const int driveSpeedValue = 20;
  static const int stayTimeValue = 60 * 10 * 1000;
  static const int calcTimeValue = 10 * 1000;

  static Future<void> addData({
    required BackgroundLocationUpdateData data,
  }) async {
    SQLiteRepository repository = SQLiteRepository();
    int time = DateTime.now().millisecondsSinceEpoch;
    double calcDistance = 0.0;
    double calcSpeed = 0.0;
    LocationModel locationModel = LocationModel.fromEmpty();
    List<LocationModel> latestBuffer = await repository.selectLatestBuffer();
    LocationModel? latestData =
        latestBuffer.length > 1 ? latestBuffer.first : null;
    StatusModel statusModel = await repository.selectStatus();

    // 이전 데이터가 있을 경우 거리를 계산함
    if (latestData != null) {
      calcDistance = PathUtils.distanceBetween(
        startLatitude: latestData.latitude,
        startLongitude: latestData.longitude,
        endLatitude: data.lat,
        endLongitude: data.lon,
      );

      calcSpeed = calcDistance * 3600;

      // 이상 데이터의 경우 무시
      if (calcDistance > 0.5) {
        calcDistance = 0;
        calcSpeed = latestData.speed;
      }
    }

    // 제자리 보정
    if (calcDistance < 0.002) {
      calcDistance = 0;
      calcSpeed = 0;
    }
    if (latestBuffer.length > 1) {
      double corDistance = PathUtils.distanceBetween(
        startLatitude: latestBuffer.last.latitude,
        startLongitude: latestBuffer.last.longitude,
        endLatitude: data.lat,
        endLongitude: data.lon,
      );
      if (corDistance < 0.01) {
        calcDistance = 0;
        calcSpeed = 0;
      }
    }

    // 데이터 생성
    locationModel = LocationModel(
      latitude: data.lat,
      longitude: data.lon,
      altitude: 0.0,
      bearing: 0,
      distance: calcDistance,
      speed: calcSpeed,
      time: time,
    );

    // 버퍼 기록
    await repository.insertBuffer(model: locationModel);
    calcSpeed =
        latestBuffer.map((e) => e.speed / 3).toList().reduce((a, b) => a + b);

    // 정산중
    if (statusModel.app == AppStatus.CALC.name) {
      if (statusModel.calcTimestamp == 0) {
        statusModel.calcTimestamp = time;
        await execute();
      } else {
        if (time - statusModel.calcTimestamp >= calcTimeValue) {
          await reset();
        }
      }
    } else {
      // 대기중
      if (statusModel.app == AppStatus.WAIT.name) {
        // 기준속도를 초과하면 운전모드 기록 시작
        if (calcSpeed >= driveSpeedValue) {
          NotificationService.send(body: '기록 시작');
          statusModel.app = AppStatus.ACTIVE.name;
          statusModel.mode = ModeStatus.DRIVE.name;
          statusModel.startTimestamp = time;
        }
      }

      // 기록중
      if (statusModel.app == AppStatus.ACTIVE.name) {
        // 기준속도를 초과하면 run 모드에서 drive 로 변경
        if (calcSpeed >= driveSpeedValue &&
            statusModel.mode == ModeStatus.RUN.name) {
          statusModel.mode = ModeStatus.DRIVE.name;
        }

        // 3km/h 이하이면 대기모드 체크
        if (calcSpeed <= 3) {
          // 대기 시간이 없으면 대기시간 기록
          if (statusModel.stayTimestamp == 0) {
            statusModel.stayTimestamp = time;
            // 대기 시간이 있으면
          } else {
            // 10분동안 머물렀는지 체크하여 정산처리
            if (time - statusModel.stayTimestamp >= stayTimeValue) {
              statusModel.app = AppStatus.CALC.name;
            }
          }

          // 3km/h를 초과하면 대기모드 초기화
        } else {
          statusModel.stayTimestamp = 0;
        }

        // 이동 거리 합산
        statusModel.distance = statusModel.distance + calcDistance;

        // 데이터 기록
        await repository.insertActive(model: locationModel);
      }

      statusModel.latitude = data.lat;
      statusModel.longitude = data.lon;
      statusModel.speed = calcSpeed;

      await repository.updateStatus(model: statusModel);
    }
  }

  static Future<void> start({required String mode}) async {
    final SQLiteRepository repository = SQLiteRepository();
    StatusModel statusModel = await repository.selectStatus();
    statusModel.app = AppStatus.ACTIVE.name;
    statusModel.mode = mode;
    statusModel.startTimestamp = DateTime.now().millisecondsSinceEpoch;
    await repository.updateStatus(model: statusModel);
  }

  static Future<void> stop() async {
    final SQLiteRepository repository = SQLiteRepository();
    StatusModel statusModel = await repository.selectStatus();
    statusModel.app = AppStatus.CALC.name;
    await repository.updateStatus(model: statusModel);
  }

  static Future<void> reset() async {
    SQLiteRepository repository = SQLiteRepository();
    await repository.dropBuffer();
    await repository.dropActive();
    await repository.updateStatus(model: StatusModel.fromEmpty());
  }

  static Future<void> execute() async {
    NotificationService.send(body: '기록 종료');
    await reset();
  }
}
